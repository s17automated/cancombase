# cancombase
50x100 mm base board for mounting inside double plug sockets connected via CAT cable to a base station that converts incoming msgs to UDP for further processing in the home automation system
- 1 pair for power
- 2 pairs for CAN

## main components 
- arduino pro mini 5 V as base 
- MCP2515 and MCP2551 connected via SPI to arduino for communication
- Powersupply with 24V input and 5V output based on buck MAX5033
- 3 PWM outputs for RGB LED directly to VNQ500 high side driver with integrated charge pump
- 6 status LED outputs via I2C PCF8574 (8 bit IO) and VNQ500 as above
- 6 push button inputs via voltage divider 

## todo

### protection circuitry

#### can:
- EMC ???
#### push buttons
- capacitors to reduce noise
#### output filter of power supply (see below)
- to reduce riplle and noise
#### temp sense 

#### ambient light sense ?
 
#### calculate load


| part         | current[mA] | coimment                           |
| ------------ | ----------: | ---------------------------------- |
|Arduino       |10           | 200mA max                          |
|MCP2515       |10           |                                    |
|MCP2551       |75           |(recessive) 200mA short             |
|9 LEDs        | 0           |(directly powered by 24V)           |
|6 Taster      | 0           |(directly powered by 24V)           |
|3 VNQ500      |25           |(3*8mA all LEDs on on all channels) |
|1 PCF8574T    |80           |(max)                               |
| ------------ | ----------: | ---------------------------------- |
|sum           |200          |                                    |

500 mA seems to be a safe choise to maintain operation during shortcut
downside: buck can not run in continuous mode so there is more ripple in normal mode
add LC combination to reduce ripple


## Calculations:

### power supply MAX5033
based on data sheet example
### optional ripple reduction with LC combination 
base on previous design with TL2575-05: 22mH and 100uF

